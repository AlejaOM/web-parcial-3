from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import login
from .forms import registerForm
from django.contrib import messages
from django.template import loader
# Create your views here.

def register(request): 
    error_message = None
    if request.method == 'POST':
        email = request.POST.get('email')
        name = request.POST.get('name')
        uname = request.POST.get('username')
        pswrd1 = request.POST.get('password1')
        pswrd2 = request.POST.get('password2')
        if User.objects.filter(username=uname).exists():
            return HttpResponse('El nombre de usuario ya está en uso')
        if pswrd1 != pswrd2:
            return HttpResponse('Las contraseñas no coinciden ')
        else:
            user = User.objects.create_user(uname,email,pswrd1)
            user.save()
            login(request, user)
            return redirect('login')

    return render(request, 'register.html', {'error_message': error_message})

    """if request.method == 'POST':
        form = registerForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registro exitoso")
            return redirect("start")
        else:
            messages.error(request, "Error al registrarse, verifique los datos")
    form = registerForm()
    context = {"form": form}
    template = loader.get_template("register.html")
    return HttpResponse(template.render(context, request))"""
