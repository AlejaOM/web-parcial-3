from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class registerForm(UserCreationForm):
    email = forms.EmailField(label='Correo:', widget=forms.TextInput, required=True)
    name = forms.CharField(label='Nombre completo:', widget=forms.TextInput, required=True)
    username = forms.CharField(label='Nombre de Usuariuo:', widget=forms.TextInput, required=True)
    password1 = forms.CharField(label='Contraseña:', widget=forms.PasswordInput, required=True)
    password2 = forms.CharField(label='Confirma contraseña:', widget=forms.PasswordInput, required=True)

    class Meta:
        model = User
        fields = ['email', 'name', 'username', 'password1', 'password2']
        help_text = {k:"" for k in fields}

    def save(self, commit=True):
        user = super(registerForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.name = self.cleaned_data['name']
        user.username = self.cleaned_data['username']  
        if commit:
            user.save()
        return user

    
