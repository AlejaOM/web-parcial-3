//Start page
document.addEventListener('DOMContentLoaded', function() {
    const notas = document.getElementById('start-page');
    const texto = notas.innerText;
    notas.innerText = '';

    for (let i = 0; i < texto.length; i++) {
        const span = document.createElement('span');
        span.innerText = texto[i];
        span.style.animation = `revealLetter 0.5s ease ${i * 0.1}s forwards`;
        notas.appendChild(span);
    }
});

//Home > notes > new note

function closeSuper() {
    document.querySelector('.super-over').style.display = 'none';
}

//Redirect logout


//window.location.replace("{% url 'login' %}");


