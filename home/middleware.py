"""from django.shortcuts import redirect

class SessionCheckMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Verifica si la sesión está cerrada y redirige a la página de inicio de sesión
        if not request.user.is_authenticated:
            return redirect('start')  # Cambia 'login' con la URL de tu página de inicio de sesión

        response = self.get_response(request)
        return response"""