# Generated by Django 4.2.6 on 2023-11-25 08:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_notes'),
    ]

    operations = [
        migrations.RenameField(
            model_name='note',
            old_name='fecha',
            new_name='date',
        ),
        migrations.DeleteModel(
            name='Notes',
        ),
    ]
