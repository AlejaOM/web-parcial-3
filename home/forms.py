from django import forms
from .models import Note

class NoteForm(forms.ModelForm):

    class Meta:
        model = Note

        fields = {
            'title',
            'note',
        }

        labels = {
            'title': 'Titulo',
            'note': 'Nota',

        }



        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control'}),
            'note': forms.Textarea(attrs={'class':'form-control','rows':6}),

        }

        def __init__(self, *args, **kwargs):
            super(NoteForm, self).__init__(*args, **kwargs)
            self.fields['title'].error_messages = {'required': 'custom required message'}
            self.fields['note'].error_messages = {'required': 'custom required message for note'}
        