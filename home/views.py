from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control, never_cache
from django.template import loader
from .models import Note
from .forms import NoteForm
from django.http import HttpResponse
from datetime import date, datetime

# Create your views here.
@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def home(request):
    notes = Note.objects.filter(user=request.user).order_by('-date')
    return render(request, 'home.html', {'notes': notes})  

@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def log_out(request):
    logout(request)
    return redirect('start')

@login_required
def new_note(request):
    
 
    if request.method == 'POST':
        form = NoteForm(request.POST or None, request.FILES)
        #user = request.user 
        #if user != request.user:
         #   return redirect('login')
        #else:
         #   new_n = Note.objects.create(title=title, note=note_text, user=user )
        #return redirect('home')
        if form.is_valid():
            new_n = form.save(commit=False)
            new_n.user = request.user
            new_n.save()
            return redirect('home')
    else:
        form = NoteForm()
    context = {}
    context['form'] = form
            
    return render(request, 'notes.html', context)

@login_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def update_note(request, id):
    obj = get_object_or_404(Note, id=id)
    form = NoteForm(request.POST or None, instance=obj)
    if request.method == 'POST':
        if form.is_valid():
            new_n = form.save(commit=False)
            new_n.date = datetime().now()
            new_n.save()
            return redirect('home')
    context = {'form': form, 'note': obj}
    return render(request, 'update.html', context)

def see_note(request, id):

    note = Note.objects.get(id=id)
    context = {'note': note}
    return render(request, 'see.html', context)


def delete_note(request, id):
    note = Note.objects.get(id=id)
    context = {'note': note}
    obj = get_object_or_404(Note, id=id)

    if request.method == 'POST':
        obj.delete()
        return redirect('home')
    return render(request, 'delete.html', context)