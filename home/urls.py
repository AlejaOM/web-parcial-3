from django.urls import path
from . import views

urlpatterns = [
    path('local/', views.home, name='home'),
    path('logout/', views.log_out, name='logout'),
    path('new_note/', views.new_note, name='new_note'),
    path('update_note/<id>/', views.update_note, name='update_note'),
    path('delete_note/<id>/', views.delete_note, name='delete_note'),
    path('see_note/<id>/', views.see_note, name='see_note'),
]