from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth import authenticate, login


# Create your views here.
def log_in(request):
    error = None
    if request.method == 'POST':
        username = request.POST.get('usern')
        password = request.POST.get('password')
        user = authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect('home')
        else:
            return render(request, 'login.html', {'error': 'Nombre de usuario o contraseña incorrectos'})
    return render(request, 'login.html')